import urllib, urllib2

def dehtml(html):
    string = ""
    flag = True
    for i in html:
        if((i == '<') or (i == '>')): flag ^= 1
        elif(flag): string += i
        if(i == '/'): string += ' '
    return string.replace('\n','').replace('\t','').lstrip().rstrip()

def artistdata(url):
    html = urllib2.urlopen(url).read()
    html = html[html.find('columns')+9:html.find('column2')]
    return dehtml(html)

id = ask("Enter the first part of Accession Number followed by pound",{"choices":"[4 DIGITS]","terminator":'#',"mode":"dtmf"})
id2 = ask("Enter second part, press star for letter ending",{"choices":"[1-3 DIGITS]","terminator":'#',"mode":"dtmf"})

number = id.value + '.' + id2.value

pm = urllib2.HTTPPasswordMgrWithDefaultRealm()
pm.add_password(None,'http://edan-api.si.edu/','HACK','hackme2013!')
h = urllib2.HTTPBasicAuthHandler(pm)
opener = urllib2.build_opener(h)
urllib2.install_opener(opener)
a = urllib2.urlopen('http://edan-api.si.edu/metadataService?q=record_ID:saam_'+number+'&wt=json')
json = a.read()

if(int(json[json.find('numFound')+10:json.find('numFound')+11]) == 0):
    say("Cannot Find Piece")
    hangup()

link = json[json.find('record_link')+14:]
link = link[:link.find('"')]

html = urllib.urlopen(link).read()

artistid = []
artists = []

title = ""
quote = ""
description = ""
location = ""
podcasts = []
choices = []

try:    
    html = html[html.find('contentArtworkDetails')+21:html.find('contentArtworkKeywords')]
    title = html[html.find('h2>')+3:html.find('/')-1]
    html = html[html.find('/')+1:]
    date = html[html.find('date">')+6:html.find('/')-1]
    
    i = html.find('artist/')
    while(i != -1):
        html = html[i+11:]
        artistid.append(html[:html.find('"')])
        artists.append(html[html.find('g>')+2:html.find('/')-1])
        i = html.find('artist/')

    html = html[html.find('location'):]
    location = dehtml(html[html.find('location')+10:html.find('</')])
    
    i = html.find('embed')
    while(i != -1):
        html = html[i+11:]
        podcasts.append("http://americanart.si.edu" + html[:html.find('.mp3')+4])
        i = html.find('embed') 
    for z in range(len(podcasts)):
        choices.append("Podcast "+str(z+1))
        
    
    if(html.find('Quote') != -1):
        html = html[html.find('Quote')+9:]
        podcasts.append(html[html.find('p')+2:html.find('/p')-1])
        choices.append("Quote")

    if(html.find('Label') != -1):
        html = html[html.find('Label')+9:]
        podcasts.append(dehtml(html[html.find('p')+2:html.find('/p')-1]))
        choices.append("Label Description")
    
    for index in range(len(artists)):
        choices.append("More about "+artists[index])
        podcasts.append(artistdata('http://americanart.si.edu/collections/search/artist/?id='+artistid[index]))
        
    prompt = ""
    for c in range(len(choices)):
        prompt += "Press "+str(c+1)+" to hear "+choices[c] + ". "
    
    say(title + ' ' + date + ' ' + location)
    ans = ask(prompt,{"choices":"[1 DIGIT]","terminator":'#',"mode":"dtmf","attempts":3})
    say(podcasts[int(ans.value)-1]);
    
except:
    say("Cannot find information")
